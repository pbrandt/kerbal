run once gui.

phase("Atmospheric Ascent").
message("").

declare local tower_height to 20.

local function tel {
  telemetry("altitude", ship:altitude).
  telemetry("radar", alt:radar).
  telemetry("apoapsis", alt:apoapsis).
  telemetry("velocity", ship:velocity:surface:mag).
  telemetry("q", ship:dynamicpressure).
  telemetry("time-to-apogee", eta:apoapsis).
}.

declare local pitchover_angle to 3.
declare local pitchover_complete_height to 500.

local function ascent_steering {
  if alt:radar > pitchover_complete_height {
    lock steering to ship:velocity:surface:direction.
  } else {
    set a to 90 - 10/(pitchover_complete_height - tower_height) * (alt:radar - tower_height).
    lock steering to heading(90, a).
  }
}.

// telemetry
clear_telemetry().
tel().

// Start first stage
message("Ignition").
stage.

// detect liftoff
until alt:radar > 1 {
  tel().
  wait 0.
}.
message("Liftoff").

// clear tower
until alt:radar > tower_height {
  tel().
  wait 0.
}
message("Cleared tower").

// first stage ascent
// starting pitch schedule with ascent_steering
until maxthrust = 0 {
  tel().
  ascent_steering().
  wait 0.1.
}.
message("First stage burnout").

// discard first stage and start second simultaneously
stage.
lock throttle to 0.8.

// second stage targets apogee
message("Targeting apogee of 70000").
until alt:apoapsis > 70000 {
  tel().
  ascent_steering().
  wait 0.1.
}.

// apogee height predicted, cut off engine
message("SECO-1").
lock throttle to 0.

// wait until time-to-apogee is like 20 seconds
until eta:apoapsis < 10 {
  tel().
  wait 0.1.
}.

// second stage reignition to circularize orbit
message("Second stage reignition").
lock throttle to 0.5.
until alt:periapsis > 70000 {
  tel().
  lock steering to prograde.
  wait 0.1.
}.

// orbit circularized
message("SECO-2").
lock throttle to 0.
