run once gui.

phase("Atmospheric Ascent").
message("").

declare local tower_height to 20.

local function tel {
  telemetry("altitude", ship:altitude).
  telemetry("radar", alt:radar).
  telemetry("apoapsis", alt:apoapsis).
  telemetry("velocity", ship:velocity:surface:mag).
  telemetry("q", ship:dynamicpressure).
}.

clear_telemetry().
tel().
stage.
message("Ignition").
tel().
wait 0.5.
tel().

// liftoff
wait until alt:radar > 1.
message("Liftoff").

// clear tower and pitch over
wait until alt:radar > tower_height.
message("Cleared tower").

// first stage
until maxthrust = 0 {
  tel().
  lock steering to up.
  wait 0.1.
}.

message("First stage burnout").

wait 1.

stage.

message("Targeting apogee of 70000").

// second stage targets apogee
until alt:apoapsis > 70000 {
  tel().
  lock steering to up.
  wait 0.1.
}.

message("SECO").

set thrust to 0.

// wait to get to the apogee
