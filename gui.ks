clearscreen.
set terminal:width to 80.

declare local line_separator to "────────────────────────────────────────────────────────────────────────────────".
declare local line_fill to "                                                                                ".
declare local telemetry_lines to lexicon().

declare local n to 0.
print "Flight Software" at(0,n). set n to n + 1.
print line_separator at(0,n). set n to n + 1.
declare local phase_line to n.
print line_fill at(0,n). set n to n + 1.
print line_separator at(0,n). set n to n + 1.
declare local message_line to n.
print line_fill at(0,n). set n to n + 1.
print line_separator at(0,n). set n to n + 1.
declare local telemetry_start to n. set n to n + 1.


function phase {
  parameter text.
  print line_fill at(0,phase_line).
  print text at(0,phase_line).
}.

function message {
  parameter text.

  print line_fill at(0,message_line).
  print text at(0,message_line).
}.

function telemetry {
  parameter key, value.

  print_telemetry(key, round(value, 2)).
}

function string_telemetry {
  parameter key, value.
  print_telemetry(key, value).
}

function print_telemetry {
  parameter key, value.

  if telemetry_lines:haskey(key) {
    // already have it, just print it at the right line later
  } else {
    telemetry_lines:add(key, telemetry_lines:length).
    print key + ":" at(0, telemetry_lines[key] + telemetry_start).
  }

  print value at(key:length + 2, telemetry_lines[key] + telemetry_start).
}.

function clear_telemetry {
  from {local i is telemetry_lines:length.} until i = 0 step {set i to i - 1.} do {
    print line_fill at(0, telemetry_start + i - 1).
  }
  set telemetry_lines to lexicon().
}.
