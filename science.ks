run once gui.
phase("Science").

clear_telemetry().
set p to ship:partstagged("sensor_temperature")[0].
set m to p:getmodule("ModuleScienceExperiment").
m:deploy.
wait until m:hasdata.
for d in m:data {
  string_telemetry(d:title, d:sciencevalue + " science (transmit value " + d:transmitvalue + ")").
}
m:transmit.
